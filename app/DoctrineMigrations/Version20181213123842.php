<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Rename blog to review and author to reviewer
 */
final class Version20181213123842 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D70BDAFD8C8');
        $this->addSql('DROP INDEX IDX_2B219D70BDAFD8C8 ON entry');
        $this->addSql(
            'ALTER TABLE entry CHANGE author reviewer INT NOT NULL,
            CHANGE blog review LONGTEXT NOT NULL');
        $this->addSql(
            'ALTER TABLE entry ADD CONSTRAINT FK_2B219D70E0472730 FOREIGN KEY (reviewer)
            REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_2B219D70E0472730 ON entry (reviewer)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE `entry` DROP FOREIGN KEY FK_2B219D70E0472730');
        $this->addSql('DROP INDEX IDX_2B219D70E0472730 ON `entry`');
        $this->addSql(
            'ALTER TABLE `entry` CHANGE reviewer author INT NOT NULL,
            CHANGE review blog LONGTEXT NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql(
            'ALTER TABLE `entry` ADD CONSTRAINT FK_2B219D70BDAFD8C8 FOREIGN KEY (author)
            REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_2B219D70BDAFD8C8 ON `entry` (author)');
    }
}
