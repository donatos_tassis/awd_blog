<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181219045334 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `rating` (id INT NOT NULL, reviewer INT NOT NULL,
            book INT NOT NULL, value INT NOT NULL, INDEX IDX_D8892622E0472730 (reviewer),
            INDEX IDX_D8892622CBE5A331 (book), PRIMARY KEY(id, reviewer, book))
             DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE `rating` ADD CONSTRAINT FK_D8892622E0472730 FOREIGN KEY (reviewer)
                 REFERENCES `user` (id)');
        $this->addSql(
            'ALTER TABLE `rating` ADD CONSTRAINT FK_D8892622CBE5A331 FOREIGN KEY (book)
            REFERENCES `book` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE `rating`');
    }
}
