<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add foreignId and change searchEntity to string
 */
final class Version20181219170859 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE search_index DROP FOREIGN KEY FK_B446A4E8B7896E2B');
        $this->addSql('DROP INDEX IDX_B446A4E8B7896E2B ON search_index');
        $this->addSql(
            'ALTER TABLE search_index ADD search_entity VARCHAR(255) NOT NULL,
                 CHANGE searchentity foreign_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE `search_index` DROP search_entity,
                 CHANGE foreign_id searchEntity INT NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE `search_index` ADD CONSTRAINT FK_B446A4E8B7896E2B 
                 FOREIGN KEY (searchEntity) REFERENCES search_entities (id)'
        );
        $this->addSql('CREATE INDEX IDX_B446A4E8B7896E2B ON `search_index` (searchEntity)');
    }
}
