<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181219133841 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `search_entities` (id INT AUTO_INCREMENT NOT NULL,
                 entityName VARCHAR(255) NOT NULL, field VARCHAR(255) NOT NULL, 
                 PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci 
                 ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE search_index DROP entityName, DROP field, CHANGE foreignid
                 searchEntity INT NOT NULL'
        );
        $this->addSql(
            'ALTER TABLE search_index ADD CONSTRAINT FK_B446A4E8B7896E2B 
                 FOREIGN KEY (searchEntity) REFERENCES `search_entities` (id)'
        );
        $this->addSql('CREATE INDEX IDX_B446A4E8B7896E2B ON search_index (searchEntity)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE `search_index` DROP FOREIGN KEY FK_B446A4E8B7896E2B');
        $this->addSql('DROP TABLE `search_entities`');
        $this->addSql('DROP INDEX IDX_B446A4E8B7896E2B ON `search_index`');
        $this->addSql(
            'ALTER TABLE `search_index` ADD entityName VARCHAR(255) 
                 NOT NULL COLLATE utf8_unicode_ci, ADD field VARCHAR(255) 
                 NOT NULL COLLATE utf8_unicode_ci, CHANGE searchentity foreignId INT NOT NULL'
        );
    }
}
