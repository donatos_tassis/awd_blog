<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add Book entity and a foreign key into Entry entity that is related with Book
 */
final class Version20181213121224 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `book` (id INT AUTO_INCREMENT NOT NULL,
            title VARCHAR(255) NOT NULL, author VARCHAR(255) NOT NULL, summary LONGTEXT NOT NULL,
            PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE entry ADD book INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE entry ADD CONSTRAINT FK_2B219D70CBE5A331 FOREIGN KEY (book) 
            REFERENCES `book` (id)');
        $this->addSql('CREATE INDEX IDX_2B219D70CBE5A331 ON entry (book)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE `entry` DROP FOREIGN KEY FK_2B219D70CBE5A331');
        $this->addSql('DROP TABLE `book`');
        $this->addSql('DROP INDEX IDX_2B219D70CBE5A331 ON `entry`');
        $this->addSql('ALTER TABLE `entry` DROP book');
    }
}
