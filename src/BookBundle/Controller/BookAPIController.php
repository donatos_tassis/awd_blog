<?php

namespace BookBundle\Controller;

use BookBundle\Entity\Book;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BookAPIController
 *
 * @package BookBundle\Controller
 */
class BookAPIController extends AbstractFOSRestController
{

    /**
     * Gets the book with given id {slug}
     * Get("/books/{slug}")
     *
     * @throws 404 `not found` if the given book id is not found.
     *
     * @param $slug
     * @return JsonResponse|Response
     */
    public function getBookAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($slug);

        if(!$book) {
            // check if the given user exists
            return new JsonResponse(
                ['error' => 'Book not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->handleView(
            $this->view($book, Response::HTTP_OK)
        );
    }
}