<?php

namespace BookBundle\Controller;

use BlogBundle\Controller\GoogleBooksService;
use BlogBundle\Entity\Entry;
use BookBundle\Entity\Book;
use BookBundle\Form\AddBookType;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->getRepository(Book::class)->getBooks();

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $books = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );

        $rating = [];
        $googleUrls = [];

        /** @var Book $book */
        foreach($books as $book) {
            $totalReviews = 0;
            $totalRating = 0;
            $bookRating = 0;

            $query = $entityManager->getRepository(Entry::class)
                ->getReviewsByBook($book);
            $reviews = $query->getResult();

            /** @var Entry $review */
            foreach ($reviews as $review) {
                $totalReviews++;
                $totalRating += $review->getRating();
            }

            if ($totalReviews !== 0) {
                $bookRating = $totalRating / $totalReviews;
            }

            $rating[$book->getId()] = $bookRating;
            $googleBooksService = $this->container->get('google.books');
            $googleUrls[$book->getId()] = $googleBooksService->getGoogleBooksUrl(
                $book->getId()
            );
        }

        return $this->render(
            'BookBundle:Default:index.html.twig',
            [
                'books' => $books,
                'rating' => $rating,
                'googleUrls' => $googleUrls
            ]
        );
    }


    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function addBookAction(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(AddBookType::class, $book);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            /** @var UploadedFile $file */
            $file = $form->get('image')->getData();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('image_directory'),$fileName);
            $book->setImage($fileName);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            // Redirects back to index page
            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render(
            '@Book/Default/add_book.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }
}
