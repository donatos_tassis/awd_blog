<?php

namespace BlogBundle\Entity;

use BookBundle\Entity\Book;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entry
 *
 * @ORM\Table(name="`entry`")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\EntryRepository")
 */
class Entry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="review", type="text", nullable=false)
     */
    private $review;

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Range(min=0 , max=5)
     *
     * @ORM\Column(name="rating", type="string")
     */
    private $rating;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;
    
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="reviews")
     * @ORM\JoinColumn(name="reviewer", referencedColumnName="id", nullable=false)
     */
    private $reviewer;

    /**
     * @var Book
     * @ORM\ManyToOne(targetEntity="BookBundle\Entity\Book", inversedBy="entries")
     * @ORM\JoinColumn(name="book", referencedColumnName="id", nullable=false)
     */
    private $book;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @return Entry
     */
    public function setTitle()
    {
        $this->title = $this->book->getTitle().' Reviewed by '.
            $this->reviewer->getFirstName().' '.$this->reviewer->getSurname();

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set review.
     *
     * @param string $review
     *
     * @return Entry
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review.
     *
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set timestamp.
     *
     * @param DateTime $timestamp
     *
     * @return Entry
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp.
     *
     * @return DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set rating
     *
     * @param int $rating
     */
    public function setRating(int $rating)
    {
        $this->rating = $rating;
    }

    /**
     * Set reviewer.
     *
     * @param User $reviewer
     */
    public function setReviewer(User $reviewer)
    {
        $this->reviewer = $reviewer;

    }

    /**
     * Get reviewer.
     *
     * @return User
     */
    public function getReviewer()
    {
        return $this->reviewer;
    }

    /**
     * Get book.
     *
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set book.
     *
     * @param Book $book
     */
    public function setBook(Book $book)
    {
        $this->book = $book;
    }
}
