<?php

namespace BlogBundle\Service;

use BookBundle\Entity\Book;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GoogleBooksService
 *
 * @package BlogBundle\Controller
 */
class GoogleBooksService
{
    /** @const string */
    const GOOGLE_API = 'https://www.googleapis.com/books/v1';

    /** @var ContainerInterface  */
    protected $container;

    /** @var EntityManager */
    protected $em;

    /**
     * GoogleBooksService constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
    }


    /**
     * Returns the Google Books url for the specified book title
     * or null if the books' title doesn't much any of the Google Books titles
     *
     * @param $id
     *
     * @return String | null
     */
    public function getGoogleBooksUrl($id)
    {
        /** @var Book $book */
        $book = $this->em->getRepository(Book::class)->find($id);
        $query = ['title' => $book->getTitle()];
        $options = [
            'query' => [
                'q' => json_encode($query),
                'orderBy' => 'relevance',
                'maxResults' => 5
            ]
        ];

        $googleBooks = $this->getRelevantBooks($options);

        $googleUrl = null;
        foreach ($googleBooks as $googleBook) {
            if(strcasecmp($googleBook['volumeInfo']['title'], $book->getTitle()) == 0) {
                $googleUrl = $googleBook['volumeInfo']['canonicalVolumeLink'];
                break;
            }
        }

        return $googleUrl;
    }

    /**
     * Gets the newest books from Google Books API
     *
     * @return array
     */
    public function getNewestBooks()
    {
        $options = [
            'query' => [
                'q' => '{}',
                'orderBy' => 'newest'
            ]
        ];

        return $this->getRelevantBooks($options);
    }

    /**
     * Returns a list of books retrieved from Google Books API
     *
     * @param array $options
     *
     * @return array
     */
    private function getRelevantBooks(array $options)
    {
        $client = new Client();

        $uri = '/volumes';
        $response = $client->get(self::GOOGLE_API . $uri, $options);

        $jsonData = $response->getBody()->getContents();
        $results = json_decode($jsonData, true);

        return $results['items'];
    }
}
