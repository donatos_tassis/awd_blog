<?php

namespace BlogBundle\Controller;

use AppBundle\Exceptions\BookNotFoundException;
use BlogBundle\Entity\Entry;
use BlogBundle\Form\EntryType;
use BookBundle\Entity\Book;
use DateTime;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Entity\User;

/**
 * Class BlogController
 *
 * @package BlogBundle\Controller
 */
class BlogController extends Controller
{
    /**
     * View of a single review chosen by id
     *
     * @param int $id
     * 
     * @return Response
     */
    public function viewAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entry = $entityManager->getRepository(Entry::class)->find($id);
        
        return $this->render(
            'BlogBundle:Blog:view.html.twig',
            [
                'entry' => $entry
            ]
        );
    }

    /**
     * Return reviews of the chosen book
     *
     * @param Request $request
     * @param int $id The book id
     *
     * @return Response
     */
    public function viewByBookAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->getRepository(Entry::class)->getReviewsByBook($id);
        $book = $entityManager->getRepository(Book::class)->find($id);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $reviews = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );

        $totalReviews = 0;
        $totalRating = 0;
        $bookRating = 0;

        /** @var Entry $review */
        foreach($reviews as $review) {
            $totalReviews++;
            $totalRating += $review->getRating();
        }

        if ($totalReviews !== 0) {
            $bookRating = $totalRating / $totalReviews;
        }

        return $this->render(
            '@Blog/Blog/viewByBook.html.twig',
            [
                'rating' => $bookRating,
                'book' => $book,
                'reviews' => $reviews
            ]
        );
    }

    /**
     * Return the specified users' reviews passed by id
     *
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     */
    public function viewByUserAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->getRepository(Entry::class)->getReviewsByUser($id);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $reviews = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $fullName = $user->getFirstName() .' '. $user->getSurname();

        return $this->render(
            '@Blog/Blog/viewMyReviews.html.twig',
            [
                'title' => 'Reviews of user '.$fullName,
                'reviews' => $reviews
            ]
        );
    }

    /**
     * Return the current users reviews
     *
     * @param Request $request
     *
     * @return Response
     */
    public function viewMyReviewsAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $query = $entityManager->getRepository(Entry::class)->getReviewsByUser($userId);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $reviews = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );


        return $this->render(
            '@Blog/Blog/viewMyReviews.html.twig',
            [
                'title' => 'My Reviews',
                'reviews' => $reviews
            ]
        );
    }

    /**
     * Creates a review entry into the Database
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     * @throws BookNotFoundException
     */
    public function createAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $book = $entityManager->getRepository(Book::class)->find($id);

        if (is_null($book)) {
            throw new BookNotFoundException();
        }

        $entry = new Entry();
        $form = $this->createForm(
            EntryType::class,
            $entry,
            [
                'action' => $request->getUri()
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $entry->setReviewer($this->getUser());
            $entry->setTimestamp(new DateTime());
            $entry->setBook($book);
            $entry->setTitle();
            $entityManager->persist($entry);
            $entityManager->flush();
            
            return $this->redirect($this->generateUrl(
                'blog_view',
                [
                    'id' => $entry->getId()
                ]
            ));
        }
        
        return $this->render(
            'BlogBundle:Blog:create.html.twig',
            [
                'form' => $form->createView(),
                'book_title' => $book->getTitle()
            ]
        );
    }

    /**
     * Updates a review entry into the Database
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entry = $entityManager->getRepository(Entry::class)->find($id);

        // blocks all user from editing the review except any admin and the user
        // that the review belongs to
        if ($entry->getReviewer() !== $this->getUser() &&
            !$this->container->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(
            EntryType::class,
            $entry,
            [
                'action' => $request->getUri()
            ]
        );

        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $entityManager->flush();
            return $this->redirect($this->generateUrl(
                'blog_view',
                [
                    'id' => $entry->getId()
                ]
            ));
        }
        
        return $this->render(
            'BlogBundle:Blog:edit.html.twig',
            [
                'form' => $form->createView(),
                'entry' => $entry
            ]
        );
    }

    /**
     * Deletes a review entry from the Database
     *
     * @param int $id
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entry = $entityManager->getRepository(Entry::class)->find($id);
        
        $entityManager->remove($entry);
        $entityManager->flush();

        // Redirects back to index page
        return $this->redirect($this->generateUrl('index'));
    }

}
