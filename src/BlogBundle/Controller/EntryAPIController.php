<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Entry;
use BlogBundle\Form\EntryType;
use BookBundle\Controller\BookAPIController;
use BookBundle\Entity\Book;
use DateTime;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

/**
 * Class EntryAPIController
 *
 * @package BlogBundle\Controller
 */
class EntryAPIController extends AbstractFOSRestController
{
    /**
     * Gets the Entries for the given user
     *
     * Get("/users/{slug}/entries")
     *
     * @param int $slug
     *
     * @throws 404 `not found` if the given user id is not found.
     *
     * @return Response
     */
    public function getEntriesAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($slug);

        if(!$user) {
            // check if the given user exists
            return new JsonResponse(
                ['error' => 'User not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $query = $em->getRepository(Entry::class)->getReviewsByUser($slug);
        $entries = $query->getResult();

        /** @var Entry $entry */
        foreach ($entries as $key => $entry)
        {
            $entries[$key] = $entry;
        }

        return $this->handleView(
            $this->view($entries, Response::HTTP_OK)
        );
    }

    /**
     * Gets the Entries for the given book
     *
     * Get("/books/{slug}/reviews")
     *
     * @param int $slug
     *
     * @throws 404 `not found` if the given book id is not found.
     *
     * @return Response
     */
    public function getReviewsAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($slug);

        if(!$book) {
            // check if the given user exists
            return new JsonResponse(
                ['error' => 'Book not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $query = $em->getRepository(Entry::class)->getReviewsByBook($slug);
        $reviews = $query->getResult();

        /** @var Entry $review */
        foreach ($reviews as $key => $review)
        {
            $reviews[$key] = $review;
        }

        return $this->handleView(
            $this->view($reviews, Response::HTTP_OK)
        );
    }

    /**
     * Creates an Entry record with the given book id and current user
     *
     * Post("/books/{id}/reviews")
     *
     * @param Request $request
     * @param int $slug
     *
     * @throws 404 `not_found` error when book id not exists
     * @throws 400 `bad_request` error when the request is not in json format
     * @throws 400 `bad_request` error when the form is not valid
     *
     * @return Response
     */
    public function postReviewsAction(Request $request, $slug)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $entry = new Entry();

        // prepare the form
        $form = $this->createForm(EntryType::class, $entry);

        // check we can parse the POST data in JSON format
        if($request->getContentType() !== 'json') {
            return new JsonResponse(
                ['error' => 'Invalid data format, it should be JSON'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // json decode the request content and pass it to the form
        $form->submit(json_decode($request->getContent(), true));

        // check that the POST data meets validations of the form
        if(!$form->isValid()) {
            // the form isn't valid so return the form along with a 400 status code
            return new JsonResponse(
                ['error' => 'Invalid data passed'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // the form is valid
        // create a new Entry entity and persist it to the database
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($slug);

        if(!$book) {
            // check if the given user exists
            return new JsonResponse(
                ['error' => 'Book not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $entry->setReviewer($user);
        $entry->setBook($book);
        $entry->setTitle();
        $entry->setTimestamp(new DateTime());
        $em->persist($entry);
        $em->flush();

        return $this->handleView(
            $this->view($entry, Response::HTTP_CREATED)
        );
    }

    /**
     * Updates the passed entry with the current user, the passed book id
     * and the passed json parameters `review` and `rating`
     *
     * Put("/books/{slug}/reviews/{id}")
     *
     * @param Request $request
     * @param int $slug
     * @param int $id
     *
     * @throws 404 `not_found` error when Book id not exists
     * @throws 404 `not_found` error when Entry id not exists
     * @throws 403 `forbidden` error when user is not authorised to update an Entry
     * @throws 400 `bad_request` error when the request is not in json format
     * @throws 400 `bad_request` error when the form is not valid
     *
     * @return Response
     */
    public function putReviewsAction(Request $request, $slug, $id)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($slug);
        // check if the given book exists
        if(!$book) {
            return new JsonResponse(
                ['error' => 'Book not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $entry = $em->getRepository(Entry::class)->find($id);
        if(!$entry) {
            return new JsonResponse(
                ['error' => 'Review not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        // check if user can update a post (only admin or the user that owns the entry)
        if($entry->getReviewer() !== $user && !in_array('ROLE_ADMIN', $user->getRoles())) {
            return new JsonResponse(
                ['error' => 'Forbidden action'],
                Response::HTTP_FORBIDDEN
            );
        }

        $updatedEntry = new Entry();
        // prepare the form
        $form = $this->createForm(EntryType::class, $updatedEntry);

        // json decode the request content and pass it to the form
        $form->submit(json_decode($request->getContent(), true));

        // check we can parse the POST data in JSON format
        if($request->getContentType() !== 'json') {
            return new JsonResponse(
                ['error' => 'Invalid data format, it should be JSON'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // check that the PUT data meets validations of the form
        if(!$form->isValid()) {
            // the form isn't valid so return the form along with a 400 status code
            return new JsonResponse(
                ['error' => 'Invalid data passed'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // the form is valid
        // update the Entry entity and persist it to the database
        $entry->setReviewer($user);
        $entry->setBook($book);
        $entry->setTitle();
        $entry->setTimestamp(new DateTime());
        $entry->setRating($updatedEntry->getRating());
        $entry->setReview($updatedEntry->getReview());
        $em->persist($entry);
        $em->flush();

        return $this->handleView(
            $this->view($entry, Response::HTTP_OK)
        );
    }
}
