<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Entry;
use BlogBundle\Form\EntryType;
use BookBundle\Entity\Book;
use DateTime;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

/**
 * Class BlogAPIController
 *
 * @package BlogBundle\Controller
 */
class BlogAPIController extends AbstractFOSRestController
{
    /** @const string */
    const ERROR = 'error';

    /**
     * Gets the list of all Entries
     *
     * Get("/blogposts")
     *
     * @return Response
     */
    public function getBlogpostsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entries = $em->getRepository(Entry::class)->findAll();

        foreach ($entries as $key => $entry)
        {
            $entries[$key] = $entry;
        }

        return $this->handleView($this->view($entries));
    }

    /**
     * Gets the Entry with specified id
     *
     * Get("/blogposts/{id}")
     *
     * @param int $id
     *
     * @throws 404 not found if the passed id is not found.
     *
     * @return Response
     */
    public function getBlogpostAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Entry $entry */
        $entry = $em->getRepository(Entry::class)->find($id);

        if(!$entry) {
            // no blog entry is found, so we return Json Response with the
            // appropriate error message and response code
            return new JsonResponse(
                [self::ERROR => 'Record not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->handleView(
            $this->view($entry,Response::HTTP_OK)
        );
    }

    /**
     * Creates an Entry record with the given user id and the given book id
     * Can only be used by admins
     *
     * @Rest\Post("/reviewers/{userId}/books/{id}/blogposts")
     *
     * @param Request $request
     * @param int $userId
     * @param int $id
     *
     * @throws 403 `forbidden` error when user is not an admin
     * @throws 404 `not_found` error when user id not exists
     * @throws 404 `not_found` error when book id not exists
     * @throws 400 `bad_request` error when the request is not in json format
     * @throws 400 `bad_request` error when the form is not valid
     *
     * @return Response
     */
    public function postUserBlogPostAction(Request $request, $userId, $id)
    {
        /** @var User $currentUser */
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        if(!in_array('ROLE_ADMIN', $currentUser->getRoles())) {
            // if current user is not an admin cannot post reviews for other user's account
            return new JsonResponse(
                [self::ERROR => 'Forbidden action'],
                Response::HTTP_FORBIDDEN
            );
        }

        // this is the user that the API is going to update the Entry with
        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->find($userId);
        // check if the user exists
        if(!$user) {
            // if there is no user return a 404 (not found) error
            return new JsonResponse(
                [self::ERROR => 'User not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);
        // check if the given book exists
        if(!$book) {
            return new JsonResponse(
                [self::ERROR => 'Book not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $entry = new Entry();

        // prepare the form
        $form = $this->createForm(EntryType::class, $entry);

        // check we can parse the POST data in JSON format
        if($request->getContentType() !== 'json') {
            return new JsonResponse(
                [self::ERROR => 'Invalid data format, it should be JSON'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // json decode the request content and pass it to the form
        $form->submit(json_decode($request->getContent(), true));

        // check that the POST data meets validations of the form
        if(!$form->isValid()) {
            // the form isn't valid so return the form along with a 400 status code
            return new JsonResponse(
                [self::ERROR => 'Invalid data passed'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // the form is valid
        // create a new Entry entity and persist it to the database
        $entry->setReviewer($user);
        $entry->setBook($book);
        $entry->setTitle();
        $entry->setTimestamp(new DateTime());
        $em->persist($entry);
        $em->flush();

        return $this->handleView(
            $this->view($entry, Response::HTTP_CREATED)
        );
    }

    /**
     * Updates the passed entry with the current user and the passed
     * json parameters `review` and `rating`
     * maintains the existing book
     *
     * Put("/blogposts/{id}")
     *
     * @param Request $request
     * @param int $id
     *
     * @throws 403 `forbidden` error when user is not authorised to update an Entry
     * @throws 404 `not_found` error when Entry id not exists
     * @throws 400 `bad_request` error when the request is not in json format
     * @throws 400 `bad_request` error when the form is not valid
     *
     * @return Response
     */
    public function putBlogpostAction(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $entry = $em->getRepository(Entry::class)->find($id);
        if(!$entry) {
            return new JsonResponse(
                [self::ERROR => 'Review not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        // check if user can update a post (only admin or the user that owns the entry)
        if($entry->getReviewer() !== $user && !in_array('ROLE_ADMIN', $user->getRoles())) {
            return new JsonResponse(
                [self::ERROR => 'Forbidden action'],
                Response::HTTP_FORBIDDEN
            );
        }

        $updatedEntry = new Entry();
        // prepare the form
        $form = $this->createForm(EntryType::class, $updatedEntry);

        // json decode the request content and pass it to the form
        $form->submit(json_decode($request->getContent(), true));

        // check we can parse the POST data in JSON format
        if($request->getContentType() !== 'json') {
            return new JsonResponse(
                [self::ERROR => 'Invalid data format, it should be JSON'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // check that the PUT data meets validations of the form
        if(!$form->isValid()) {
            // the form isn't valid so return the form along with a 400 status code
            return new JsonResponse(
                [self::ERROR => 'Invalid data passed'],
                Response::HTTP_BAD_REQUEST
            );
        }

        // the form is valid
        // update the Entry entity and persist it to the database
        $entry->setReviewer($user);
        $entry->setBook($entry->getBook());
        $entry->setTitle();
        $entry->setTimestamp(new DateTime());
        $entry->setRating($updatedEntry->getRating());
        $entry->setReview($updatedEntry->getReview());
        $em->persist($entry);
        $em->flush();

        return $this->handleView(
            $this->view($entry, Response::HTTP_OK)
        );
    }

    /**
     * Deletes the entry with the given id
     *
     * Delete("/blogposts/{id}")
     *
     * @param $id
     *
     * @throws 403 `forbidden` error when user is not authorised to delete an Entry
     * @throws 404 `not_found` error when Entry id not exists
     *
     * @return Response
     */
    public function deleteBlogpostAction($id)
    {
        /** @var User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        // only admin can delete a post
        if(!in_array('ROLE_ADMIN', $user->getRoles())) {
            return new JsonResponse(
                [self::ERROR => 'Forbidden action'],
                Response::HTTP_FORBIDDEN
            );
        }

        $em = $this->getDoctrine()->getManager();
        /** @var Entry $entry */
        $entry = $em->getRepository(Entry::class)->find($id);

        if(!$entry) {
            // no blog entry is found, so we return Json Response with the
            // appropriate error message and response code
            return new JsonResponse(
                [self::ERROR => 'Record not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        $em->remove($entry);
        $em->flush();

        return $this->handleView(
            $this->view($entry,Response::HTTP_OK)
        );
    }
}
