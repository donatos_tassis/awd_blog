<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GoogleBooksController
 *
 * @package BlogBundle\Controller
 */
class GoogleBooksController extends Controller
{
    /**
     * @return Response
     */
    public function viewNewestBooksAction()
    {
        $googleBooks = $this->get('google.books')->getNewestBooks();

        $books = [];
        foreach ($googleBooks as $googleBook) {

            if(!array_key_exists('volumeInfo', $googleBook))
                break;

            $bookInfo = $googleBook['volumeInfo'];
            $book = [];

            if(array_key_exists('title', $bookInfo))
                $book['title'] = $bookInfo['title'];

            if(array_key_exists('authors', $bookInfo) && !empty($bookInfo['authors']))
                $book['author'] = $bookInfo['authors'][0];

            if(array_key_exists('description', $bookInfo))
                $book['summary'] = $bookInfo['description'];

            if(array_key_exists('imageLinks',$bookInfo) && array_key_exists('smallThumbnail', $bookInfo['imageLinks']))
                $book['image'] = $googleBook['volumeInfo']['imageLinks']['smallThumbnail'];

            if(array_key_exists('canonicalVolumeLink', $bookInfo))
                $book['googleUrl'] = $googleBook['volumeInfo']['canonicalVolumeLink'];

            if(array_key_exists('publishedDate', $bookInfo))
                $book['published'] = $googleBook['volumeInfo']['publishedDate'];

            if(!empty($book))
                $books[] = $book;
        }

        return $this->render(
            '@Blog/Google/googleBooks.html.twig',
            [
                'books' => $books,
            ]
        );
    }
}
