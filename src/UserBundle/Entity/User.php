<?php

namespace UserBundle\Entity;

use BlogBundle\Entity\Entry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name", type="string", nullable=false)
     */
    private $firstName;

    /**
     * @ORM\Column(name="surname", type="string", nullable=false)
     */
    private $surname;

    /**
     * @var ArrayCollection
     * @Serializer\Exclude()
     * @ORM\OneToMany(targetEntity="BlogBundle\Entity\Entry", mappedBy="reviewer")
     */
    protected $reviews;

    /**
     * User constructor.
     *
     * @param ArrayCollection $entries
     */
    public function __construct(ArrayCollection $entries = null)
    {
        parent::__construct();

        $this->reviews = new ArrayCollection();
    }


    /**
     * Overrides the parent function to set username same with email
     * because username in FOSUserBundle is mandatory.
     * 
     * @param string $email
     * 
     * @return $this|static
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * Add review.
     *
     * @param Entry $review
     *
     * @return User
     */
    public function addReview(Entry $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review.
     *
     * @param Entry $review
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeReview(Entry $review)
    {
        return $this->reviews->removeElement($review);
    }

    /**
     * Get reviews.
     *
     * @return Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }
}
