<?php

namespace UserBundle\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

/**
 * Class UserAPIController
 *
 * @package UserBundle\Controller
 */
class UserAPIController extends AbstractFOSRestController
{
    /**
     * Gets the user with the given id {slug}
     * Get("/users/{id}")
     * @param $slug
     *
     * @throws 404 not found if the given user id is not found.
     *
     * @return JsonResponse|Response
     */
    public function getUserAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($slug);

        if(!$user) {
            // check if the given user exists
            return new JsonResponse(
                ['error' => 'User not found'],
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->handleView(
            $this->view($user, Response::HTTP_OK)
        );
    }
}
