<?php

namespace AppBundle\Twig\Extension;

use ReflectionClass;
use Twig_Extension;
use Twig_SimpleTest;

class LayoutExtension extends Twig_Extension
{
    public function getTests()
    {
        return [
            new Twig_SimpleTest('instanceof',[$this, 'isInstanceOf'])
        ];
    }

    public function isInstanceOf($object, $class)
    {
        $reflectionClass = new ReflectionClass($class);

        return $reflectionClass->isInstance($object);
    }
}