<?php

namespace AppBundle\Exceptions;

use Exception;
use Throwable;

class BookNotFoundException extends Exception implements CustomExceptionInterface
{
    /** @const string */
    const BOOK_NOT_FOUND = 'Book not found';

    public function __construct(
        string $message = self::BOOK_NOT_FOUND,
        int $code = 500,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}