<?php

namespace SearchBundle\Command;

use SearchBundle\Entity\SearchEntities;
use SearchBundle\Entity\SearchIndex;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SearchIndexPopulateCommand extends ContainerAwareCommand
{
    const COMMAND_DESCR = 'It populates the Search Index table ';

    const COMMAND_HELP = 'This command populates the search index according '.
                         'to the `search_entities` entity';

    protected function configure()
    {
        $this
            ->setName('search:index:populate')
            ->setDescription(self::COMMAND_DESCR)
            ->setHelp(self::COMMAND_HELP);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Populating the search index...');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->truncateSearchIndexTable();

        $searchableTerms = $em->getRepository(SearchEntities::class)
            ->getSearchEntities();

        /** @var SearchEntities $searchableTerm */
        foreach($searchableTerms as $searchableTerm) {
            $em->getRepository(SearchIndex::class)->createSearchIndex(
                $searchableTerm->getEntityName(),
                $searchableTerm->getField()
            );
        }

        $output->writeln('DONE');
    }

    /**
     * truncate table SearchIndex before populate it
     */
    protected function truncateSearchIndexTable()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $truncate = $platform->getTruncateTableSQL('search_index', true);
        $connection->executeUpdate($truncate);
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }

}
