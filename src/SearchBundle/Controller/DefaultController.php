<?php

namespace SearchBundle\Controller;

use BlogBundle\Entity\Entry;
use BookBundle\Entity\Book;
use Knp\Component\Pager\Paginator;
use SearchBundle\Entity\SearchIndex;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * Returns the search results
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $searchTerm = $request->get('search');
        $queryResults = $this->getDoctrine()->getRepository(SearchIndex::class)
            ->getSearchResults($searchTerm);

        $books = [];
        $users = [];
        $ratings = [];
        /** @var SearchIndex $result */
        foreach ($queryResults as $result) {
            $entity = $this->getDoctrine()
                ->getRepository($result->getSearchEntity())
                ->find($result->getForeignId());

            if($entity instanceof User && !array_key_exists($entity->getId(), $users)) {
                $users[$entity->getId()] = $entity;
            }

            if($entity instanceof Book && !array_key_exists($entity->getId(), $books)) {
                $books[$entity->getId()] = $entity;
                $bookReviews = $this->getDoctrine()
                    ->getRepository(Entry::class)
                    ->getReviewsByBook($entity->getId())
                    ->getResult();

                $totalReviews = 0;
                $totalRating = 0;

                /** @var Entry $bookReview */
                foreach($bookReviews as $bookReview) {
                    $totalReviews++;
                    $totalRating += $bookReview->getRating();
                }

                if ($totalReviews !== 0) {
                    $bookRating = $totalRating / $totalReviews;
                    $ratings[] = $bookRating;
                }
            }
        }

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagedResults = $paginator->paginate(
            array_merge($users, $books),
            $request->query->getInt('page', 1)
        );

        return $this->render(
            'SearchBundle:Default:index.html.twig',
            [
                'ratings' => $ratings,
                'results' => $pagedResults
            ]
        );
    }
}
