<?php

namespace SearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SearchIndex
 *
 * @ORM\Table(name="`search_index`")
 * @ORM\Entity(repositoryClass="SearchBundle\Repository\SearchIndexRepository")
 */
class SearchIndex
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="search_entity", type="string", nullable=false)
     */
    private $searchEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="foreign_id", type="integer", nullable=false)
     */
    private $foreignId;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $searchTerm;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entityName.
     *
     * @param string $searchEntity
     *
     * @return SearchIndex
     */
    public function setSearchEntity($searchEntity)
    {
        $this->searchEntity = $searchEntity;

        return $this;
    }

    /**
     * Get entityName.
     *
     * @return string
     */
    public function getSearchEntity()
    {
        return $this->searchEntity;
    }

    /**
     * Get foreignId.
     *
     * @return int
     */
    public function getForeignId()
    {
        return $this->foreignId;
    }

    /**
     * Set foreignId.
     *
     * @param int $foreignId
     */
    public function setForeignId(int $foreignId)
    {
        $this->foreignId = $foreignId;
    }

    /**
     * Set content.
     *
     * @param string $searchTerm
     *
     * @return SearchIndex
     */
    public function setSearchTerm($searchTerm)
    {
        $this->searchTerm = $searchTerm;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }
}
